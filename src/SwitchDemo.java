public class SwitchDemo {
	public static void main(String[] args) {
		int month = 8;

		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.println("31 Days");
			break;
		case 2:
			System.out.println("28/29 Days");
			break;

		default:
			System.out.println("Invalid Month, Please try again!");
			break;
			
		case 4: case 6:
		case 9:
		case 11:
			System.out.println("30 Days");
			break;

		}
	}

	public static void main1(String[] args) {
		int month = 1;

		switch (month) {

		case 3:
			System.out.println("Mar");
			break;

		case 1:
			System.out.println("Jan");
			break;

		case 2:
			System.out.println("Feb");
			break;

		case 4:
			System.out.println("Apr");
			break;

		case 5:
			System.out.println("May");
			break;

		case 6:
			System.out.println("Jun");
			break;

		case 7:
			System.out.println("Jul");
			break;

		case 8:
			System.out.println("Aug");
			break;

		case 9:
			System.out.println("Sep");
			break;

		case 10:
			System.out.println("Oct");
			break;

		case 11:
			System.out.println("Nov");
			break;

		case 12:
			System.out.println("Dec");
		}
	}
}
