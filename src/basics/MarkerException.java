package basics;

public class MarkerException extends RuntimeException{
	public MarkerException() {
		super();
	}
	
	public MarkerException(String m) {
		super(m);
	}
	
	public MarkerException(Throwable t) {
		super(t);
	}
	
	public MarkerException(String m, Throwable t) {
		super(m, t);
	}
}
