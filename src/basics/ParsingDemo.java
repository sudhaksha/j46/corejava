package basics;

public class ParsingDemo {
	public static void main(String[] args) {
		String s1 = "256";
		
		int i = xyz(s1);
		
		System.out.println(i * 2);
	}
	
	public static int xyz(String v) {
		int result = 0;
		int temp = 0;
		
		
		for(int i=v.length()-1, j=1; i>=0; i--, j*=10) {
			switch(v.charAt(i)) {
			case '0':
				temp = 0;
				break;
			case '1':
				temp = 1;
				break;
			case '2':
				temp = 2;
				break;
			case '3':
				temp = 3;
				break;
			case '4':
				temp = 4;
				break;
			case '5':
				temp = 5;
				break;
			case '6':
				temp = 6;
				break;
			case '7':
				temp = 7;
				break;
			case '8':
				temp = 8;
				break;
			case '9':
				temp = 9;
				break;
			}
			
			result = result + temp * j;
		}
		
		return result;
	}
}
