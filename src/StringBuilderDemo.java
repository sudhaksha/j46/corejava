
public class StringBuilderDemo {
	public static void main(String[] args) {
		StringBuilder s1 = new StringBuilder("2010, 2012, 2020, 2018, 2030");
		
		System.out.println(s1);
		
		int i;
		
		while( (i = s1.indexOf("0")) != -1 ) {
			s1.replace(i, i+1, "x");
		}
		
		System.out.println(s1);
		
		
		int count = 0;
		while( (i = s1.lastIndexOf("x")) != -1 ) {
			count++;
			
			s1.replace(i, i+1, "X");
			
			if(count == 4) {
				break;
			}
		}
	
		System.out.println(s1);
	}
}
