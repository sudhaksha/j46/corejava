
public class ArraysDemo {
	public static void main(String[] args) {
		char[] c = new char[26];
		
		for(int i=0; i<c.length; i++) {
			c[i] = (char)(i + 'A');
		}
		
		for(int i=0; i<c.length; i++) {
			System.out.print("[" + c[i] + "-" + (c[i] + 0) + "] ");
		}
	}
	
	public static void main3(String[] args) {
		int[][] a = {{10, 'A'}, {14, 16}, {18, 11}};
		
		System.out.println(a);
		
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	
	public static void main2(String[] args) {
		int a[] = {10, 22, 68, 31};
		
		int temp = 0;
		
		for(int i=0; i<a.length; i++) {
			temp += a[i];
		}
		
		System.out.println(temp);
	}
	
	public static void main1(String[] args) {
		int a[];
		
		//a = {10, 20};
		a = new int[3];
		
		for(int i=0; i<a.length; i++) {
			System.out.print(a[i] + " ");
		}
		
		a[2] = 256;
		
		System.out.println();
		for(int i=0; i<a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}
}
