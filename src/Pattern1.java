
public class Pattern1 {
	public static void main(String[] args) {
		int x = 8;
		
		for(int i=1; i<=x; i++) {
			for(int j=i; j<x; j++) {
				System.out.print(" ");
			}
			
			for(int j=1; j<=i*2; j++) {
				if(j%2 == 1) {
					System.out.print("*");
				}else {
					System.out.print(" ");
				}
			}
			
			System.out.println();
		}
	}
}
