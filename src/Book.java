import java.util.Objects;

public class Book {
	public final String TITLE;
	public final String author;
	private double price;
	private final int NUMBER_OF_PAGES;
	private int currentPage;
	public static final int MINIMUM_PAGES;

	static {
		//MINIMUM_PAGES = 50;
		
		if(System.getenv("os").toUpperCase().contains("WINDOWS")) {
			MINIMUM_PAGES = 100;
		}else {
			MINIMUM_PAGES = 50;
		}
	}

	{
		currentPage = (int) (Math.random() * MINIMUM_PAGES);
	}

	public Book(String TITLE, double price) {
		NUMBER_OF_PAGES = 200;
		this.TITLE = TITLE;
		author = "";
		this.price = price;
		// currentPage = 0;
	}

	public Book(String TITLE, double price, String author) {
		NUMBER_OF_PAGES = 200;
		this.TITLE = TITLE;
		this.author = author;
		this.price = price;
		// currentPage = 0;
	}

	public Book(String TITLE, double price, String author, int NUMBER_OF_PAGES) {
		this.NUMBER_OF_PAGES = NUMBER_OF_PAGES;
		this.TITLE = TITLE;
		this.author = author;
		this.price = price;
		// currentPage = 0;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public String getTITLE() {
		return TITLE;
	}

	public String getAuthor() {
		return author;
	}

	public int getNUMBER_OF_PAGES() {
		return NUMBER_OF_PAGES;
	}

	public int getMINIMUM_PAGES() {
		return MINIMUM_PAGES;
	}

}
