public class IfDemo {
	public static void main(String[] args) {
		int marks = 58;
		
		if(marks >= 75) {
			System.out.println("Distinction");
		}
		
		if(marks >= 60 && marks < 75) {
			System.out.println("First Class");
		}
		
		if(marks >= 50 && marks < 60) {
			System.out.println("Second Class");
		}
		
		if(marks >= 40 && marks < 50) {
			System.out.println("Third Class");
		}
		
		if(marks < 40) {
			System.out.println("Failed");
		}
	}
	
	public static void main1(String[] args) {
		int marks = 57;
		
		if(marks > 40) {
			System.out.println("Passed");
		}
	}
}
