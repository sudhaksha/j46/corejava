
public class MethodsDemo {
	public static void main(String[] args) {
		int x, y, k;
		
		x = 2;
		y = 3;
		
		printline(50, '*');
		System.out.println("James Gosling created Java Programming Language");
		printline(50, '*');
		
		printline(30);
		
		k = power(x, y);
		System.out.println(k);
		printline();
		
		k = power(3, 4);
		System.out.println(k);
		printline();
		
		k = power(x+y, 4);
		System.out.println(k);
		printline(30);
		
		printline(50, '~');
		System.out.println("                    THE END ");
		printline(50, '~');
	}
	
	public static void printline(int length, char c) {
		for(int i=0; i<length; i++) {
			System.out.print(c);
		}
		System.out.println();		
	}
	
	public static void printline(int length) {
		for(int i=0; i<length; i++) {
			System.out.print("-");
		}
		System.out.println();
	}
	
	public static void printline() {
		for(int i=0; i<25; i++) {
			System.out.print("-");
		}
		System.out.println();
	}
	
	public static int power(int a, int b) {
		int r = a;
		
		for(int i=0; i<b-1; i++) {
			r *= a;
		}
		
		return r;
	}
}
