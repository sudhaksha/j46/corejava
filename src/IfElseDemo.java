
public class IfElseDemo {
	public static void main(String[] args) {
		int marks = 68;
		
		if(marks >= 75) {
			System.out.println("Distinction");
		}else if(marks >= 60) {
			System.out.println("First Class");
		}else if(marks >= 50) {
			System.out.println("Second Class");
		}else if(marks >= 40) {
			System.out.println("Third Class");
		}else {
			System.out.println("Failed");
		}
	}
	
	public static void main2(String[] args) {
		int marks = 62;
		
		if(marks >= 75) {
			System.out.println("Distinction");
		}else {
			if(marks >= 60) {
				System.out.println("First Class");
			}else {
				if(marks >= 50) {
					System.out.println("Second Class");
				}else {
					if(marks >= 40) {
						System.out.println("Third Class");
					}else {
						System.out.println("Failed");
					}
				}
			}
		}
	}
	
	public static void main1(String[] args) {
		int marks = 34;
		
		if(marks >= 40) {
			System.out.println("Passed");
		}else {
			System.out.println("Failed");
		}
		
		if(marks % 2 == 0) {
			System.out.println("Even");
		}else {
			System.out.println("Odd");
		}
	}
}
