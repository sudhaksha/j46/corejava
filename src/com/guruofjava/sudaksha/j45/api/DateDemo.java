package com.guruofjava.sudaksha.j45.api;

import java.util.Date;

public class DateDemo {
	public static void main(String[] args) {
		Date d1 = new Date(10 * 365 * 24 * 60 * 60 * 1000L);
		
		System.out.println(d1);
		System.out.println(d1.getTime());
	}
	
	public static void main1(String[] args) {
		Date d;
		
		d = new Date();
		
		System.out.println(d);
		System.out.println(d.getTime());
		
		Date d2 = new Date();
		
		System.out.println(d2.getTime());
		
		System.out.println(d.after(d2));
		System.out.println(d.before(d2));
	}
}
