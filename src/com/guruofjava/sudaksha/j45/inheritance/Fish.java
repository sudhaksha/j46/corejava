package com.guruofjava.sudaksha.j45.inheritance;

public class Fish extends Animal2{
	@Override
	public void move(int distance) {
		System.out.println("Fish is moving " + distance + " distance");
	}
}
