package com.guruofjava.sudaksha.j45.inheritance;

public class InheritanceDemo {
	public static void main(String[] args) {
		Animal a1 = new Animal();
		a1.move(45);
		
		Cat c1 = new Cat();
		c1.move(65);
		
		Bird b1 = new Bird();
		b1.move(97);
	}
}
