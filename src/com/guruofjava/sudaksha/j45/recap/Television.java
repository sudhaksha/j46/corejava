package com.guruofjava.sudaksha.j45.recap;

public class Television extends Object {
	public static String CATEGORY = "Electronics";
	
	private final byte MAX_VOLUME = 100;
	private final short MAX_CHANNEL = 1000;

	public final String BRAND_NAME;
	public final double SIZE;
	private double price;
	private byte volume;
	private short channel;
	
	/*public static void setCategory(String CATEGORY) {
		this.CATEGORY = CATEGORY;
	}*/

	/*
	 * public Television() { BRAND_NAME = "ONIDA"; SIZE = 16; price = 8500; }
	 */

	public Television(String b) {
		super();
		BRAND_NAME = b;
		SIZE = 16;
		price = 8500;
	}

	public Television(String b, double s) {
		BRAND_NAME = b;
		SIZE = s;
		price = 18500;
	}

	public Television(String b, double s, double p) {
		BRAND_NAME = b;
		SIZE = s;
		price = p;
	}

	public void setChannel(short channel) {
		if (channel > 0 || channel < MAX_CHANNEL) {
			this.channel = channel;
		}
	}

	public void nextChannel() {
		if (channel < MAX_CHANNEL) {
			channel++;
		}
	}

	public void previousChannel() {
		if (channel > 0) {
			channel--;
		}
	}

	public byte getVolume() {
		return volume;
	}

	public void decreaseVolume() {
		if (volume > 0) {
			volume--;
		}
	}

	public void increaseVolume() {
		if (volume < MAX_VOLUME) {
			volume++;
		}
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if (price >= 4000) {
			this.price = price;
		} else {
			System.out.println("Invalid price");
		}
	}

}
