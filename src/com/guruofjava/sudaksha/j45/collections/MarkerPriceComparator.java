package com.guruofjava.sudaksha.j45.collections;

import java.util.Comparator;

import basics.Marker;

public class MarkerPriceComparator implements Comparator{
	@Override
	public int compare(Object a, Object b) {
		Marker m = (Marker)a;
		Marker n = (Marker)b;
		
		if(m.getPrice() == n.getPrice()) {
			return 0;
		}else {
			//return (int)(m.getPrice() - n.getPrice());
			if(m.getPrice() - n.getPrice() > 0) {
				return 1;
			}else {
				return -1;
			}
		}
	}
}
