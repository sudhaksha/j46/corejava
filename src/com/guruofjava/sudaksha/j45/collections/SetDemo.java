package com.guruofjava.sudaksha.j45.collections;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import basics.Marker;

public class SetDemo {
	public static void main(String[] args) {
		Set s1 = new TreeSet(new MarkerPriceComparator());
		
		s1.add(new Marker("Camlin", "Red", 22));
		s1.add(new Marker("Reynolds", "Green", 22.5));
		s1.add(new Marker("Parker", "Black", 29));
		
		System.out.println(s1);
	}
	
	public static void main6(String[] args) {
		Set s1 = new TreeSet();
		
		s1.add(new Marker("Camlin", "Red", 22));
		s1.add(new Marker("Reynolds", "Green", 25));
		s1.add(new Marker("Parker", "Black", 29));
		
		System.out.println(s1);
	}
	
	public static void main5(String[] args) {
		TreeSet s1 = new TreeSet(Arrays.asList(16, 82, 54, 32, 96));
		
		System.out.println(s1);
		System.out.println(s1.first());
		System.out.println(s1.last());
		
		//System.out.println(s1.headSet(54));
		Set s2 = s1.headSet(54);
		System.out.println(s2);
		
		System.out.println(s1.tailSet(54));
		
		System.out.println(s1.subSet(32, 82));
	}
	
	public static void main4(String[] args) {
		Set s2 = Set.of(55, 66, 33, 22, 88);
		Set s1 = new LinkedHashSet();
		s1.add(25);
		s1.add(28);
		s1.add(54);
		
		s1.addAll(Arrays.asList(15, 20));		
		System.out.println(s1);
		
		s1.addAll(Set.of(10, 92, 31));
		System.out.println(s1);
				
		
		for(Object t: s1) {
			System.out.println(t);
		}
		
		System.out.println("--");
		
		Iterator it = s1.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
	
	public static void main3(String[] args) {
		Set s1 = new LinkedHashSet();
		Set s2 = new LinkedHashSet();

		s1.add(25);
		s1.add(28);
		s1.add(54);
		
		s2.add(86);
		s2.add(28);
		s2.add(89);
		
		System.out.println(s1);
		System.out.println(s2);
		
		//s1.addAll(s2);
		//s1.removeAll(s2);
		//s1.retainAll(s2);
		System.out.println(s1.containsAll(s2));
		System.out.println(s1);
		System.out.println(s2);
	}
	
	public static void main2(String[] args) {
		Set s1 = new LinkedHashSet();

		System.out.println(s1);
		s1.add(25);
		s1.add(28);

		System.out.println(s1);
		System.out.println(s1.isEmpty());
		System.out.println(s1.size());
		System.out.println(s1.contains(32));
		
		s1.clear();
		System.out.println(s1.size());
		
		s1.add(22);
		s1.add(65);
		s1.add(54);
		
		System.out.println(s1);
		
		System.out.println(s1.remove(65));
		System.out.println(s1);
	}

	public static void main1(String[] args) {
		HashSet s1 = new HashSet();

		System.out.println(s1);
		s1.add(25);
		s1.add(28);

		System.out.println(s1);

		workWithSet(s1);
	}

	public static void workWithSet(Set s) {

	}
}
