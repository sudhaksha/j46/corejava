package com.guruofjava.sudaksha.j45.collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapDemo {
	public static void main(String[] args) {
		Map m1 = new HashMap();
		
		System.out.println(m1);
		
		m1.put("James", "Java");
		m1.put("Dennis", "Sea");
		m1.put("Stroustrup", "Sea++");
		m1.put("Joshua", "Collections");
		m1.put("Johnson", "Spring");
		
		System.out.println(m1);
		
		Set s1 = m1.keySet();
		System.out.println(s1);
		
		Collection c1 = m1.values();
		System.out.println(c1);
		
		System.out.println("-------------------------------");
		Set<Map.Entry> e1 = m1.entrySet();
		
		for(int i=0; i<47; i++) {
			System.out.print('+');
		}
		System.out.println();
		
		for(Map.Entry t: e1) {
			//System.out.println(t.getKey() + " : " + t.getValue());
			
			System.out.format("+ %-20s | %-20s +%n", t.getKey(), t.getValue());
		}
		
		for(int i=0; i<47; i++) {
			System.out.print('+');
		}
		System.out.println();
	}
}
