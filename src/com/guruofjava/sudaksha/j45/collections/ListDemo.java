package com.guruofjava.sudaksha.j45.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListDemo {
	
	public static void main(String[] args) {
		ArrayList<String> t1 = new ArrayList( Arrays.asList("James", "Gosling", "created", "Java", 
				"Language") );
		
		t1.trimToSize();
		
		
		System.out.println(t1);
		
		String str = "";
		
		String[] values = t1.toArray(new String[0]);
		
		for(int i=0; i<values.length; i++) {
			str = str + " " + values[i];
		}
		
		str = str.trim();
		
		System.out.println(str);
	}
	
	public static void main4(String[] args) {
		List<Integer> t1 = new ArrayList( Arrays.asList(10, 18, 22, 66, 14, 19, 18, 12) );
		
		Object[] a1 = t1.toArray();
		
		int sum = 0;
		
		for(int i=0; i<a1.length; i++) {
			sum = sum + (int)(a1[i]);
		}
		
		System.out.println(sum);
		
		Integer[] a2 = t1.toArray(new Integer[0]);
		
		int sum2 = 0;
		
		for(int i=0; i<a2.length; i++) {
			sum2 = sum2 + a2[i];
		}
		
		System.out.println(sum2);
	}
	
	public static void main3(String[] args) {
		List<Integer> t1 = new ArrayList( Arrays.asList(10, 18, 22, 66, 14, 19, 18, 12) );
		
		System.out.println(t1.get(4));
		
		System.out.println(t1.indexOf(18));
		System.out.println(t1.lastIndexOf(18));
		
		System.out.println(t1);
		System.out.println(t1.subList(2, t1.size()-2));
		System.out.println(t1.subList(2, 6));
				
	}
	
	public static void main2(String[] args) {
		List<Integer> t1 = new ArrayList( Arrays.asList(10, 18, 22, 66, 14, 19, 18, 12) );
		
		System.out.println(t1);
		
		Iterator<Integer> it = t1.iterator();
		
		while(it.hasNext()) {
			Integer t = it.next();
			
			if(t >= 15 && t < 50) {
				it.remove();
			}
		}
		
		System.out.println(t1);
	}
	
	public static void main1(String[] args) {
		List t1 = new ArrayList();
		
		System.out.println(t1.isEmpty());
		t1.add(20);
		System.out.println(t1.isEmpty());
		System.out.println(t1);
		
		t1.add(18);
		t1.add(20);
		t1.add(33);
		System.out.println(t1);
		
		t1.add(2, 55);
		System.out.println(t1);
		
		t1.set(2, 66);
		System.out.println(t1);
		
		t1.remove(Integer.valueOf(20));
		System.out.println(t1);
		
		System.out.println("----------------------");
		
		ListIterator it = t1.listIterator();
		
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		System.out.println("--");
		while(it.hasPrevious()) {
			System.out.println(it.previous());
		}
	}
}
