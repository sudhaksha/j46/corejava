package com.guruofjava.sudaksha.j45.collections;

import java.util.Vector;

public class LegacyDemo {
	public static void main(String[] args) {
		Vector v1 = new Vector();
		
		v1.add(25);
		v1.addElement(30);
		
		System.out.println(v1);
		
		System.out.println(v1.elementAt(1));
		System.out.println(v1.get(1));
	}
}
