package com.guruofjava.sudaksha.j45.collections;

import java.util.LinkedList;
import java.util.Queue;

public class QueueDemo {
	public static void main(String[] args) {
		Queue t1 = new LinkedList();
		
		t1.offer(15);
		t1.offer(25);
		t1.offer(45);
		t1.offer(18);
		
		System.out.println(t1);
		
		System.out.println(t1.poll());
		System.out.println(t1);
		System.out.println(t1.peek());
		System.out.println(t1);
	}
	
	public static void main1(String[] args) {
		Queue t1 = new LinkedList();
		
		t1.add(15);
		t1.add(25);
		t1.add(45);
		t1.add(18);
		
		System.out.println(t1);
		
		System.out.println(t1.remove());
		System.out.println(t1);
		System.out.println(t1.element());
		System.out.println(t1);
	}
}
