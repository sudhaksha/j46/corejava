package com.guruofjava.sudaksha.j45.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;

public class JDBCDemo {
	public static void main(String[] args) throws Exception{
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

			try{
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "root");
				st = con.createStatement();
				rs = st.executeQuery("SELECT * FROM country");

				while(rs.next()){
					int id = rs.getInt(1);
					String name = rs.getString(2);

					System.out.format("%5d : %-30s", id, name);
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}finally{
				if(Objects.nonNull(rs)){
					try{
						rs.close();
					}catch(Exception ex){
					}
				}
				if(Objects.nonNull(st)){
					try{
						st.close();
					}catch(Exception ex){
					}
				}
				if(Objects.nonNull(con)){
					try{
						con.close();
					}catch(Exception ex){
					}
				}
			}
	}
}
