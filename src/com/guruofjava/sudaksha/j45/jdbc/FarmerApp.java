package com.guruofjava.sudaksha.j45.jdbc;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Objects;

public class FarmerApp {
	private static BufferedReader input;

	private static Connection con;
	private static Statement st;
	private static ResultSet rs;

	private static final String JDBC_DRIVER_CLASS;
	private static final String JDBC_URL;
	private static final String DB_USER;
	private static final String DB_PWD;

	static {
		JDBC_DRIVER_CLASS = "com.mysql.jdbc.Driver";
		JDBC_URL = "jdbc:mysql://localhost:3306/agri_db";
		DB_USER = "root";
		DB_PWD = "root";

		try {
			Class.forName(JDBC_DRIVER_CLASS);

			input = new BufferedReader(new InputStreamReader(System.in));
		} catch (Exception e) {

		}
	}

	public static void main(String[] args) {
		String option = "";

		try {
			con = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PWD);

			while (true) {
				printMenu();

				option = input.readLine();

				switch (option) {
				case "1":
					viewFarmers();
					break;
				case "2":
					addFarmer();
					break;
				case "3":
					updateFarmer();
					break;
				case "4":
					removeFarmer();
					break;
				case "5":
					return;
				default:
					System.out.println("Invalid input option, please try again");
					input.readLine();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (Objects.nonNull(con)) {
				try {
					con.close();
				} catch (Exception e) {

				}
			}
		}
	}

	private static void removeFarmer() {
		try (Statement statement = con.createStatement()) {
			System.out.print("\nEnter the ID of the farmer to be removed: ");

			int farmerId = Integer.parseInt(input.readLine());

			statement.executeUpdate("DELETE FROM farmers WHERE farmer_id=" + farmerId);

			printLine('=', 50);
			System.out.println("Farmer Record REMOVED Successfully");
			printLine('=', 50);
			input.readLine();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void updateFarmer() {
		try (Statement st = con.createStatement()) {
			System.out.print("\nEnter the farmer ID to be updated: ");
			int farmerId = Integer.parseInt(input.readLine());

			ResultSet rs = st.executeQuery("SELECT * FROM farmers WHERE farmer_id=" + farmerId);

			if (rs.next()) {
				String oldName = rs.getString(2);
				String oldEmail = rs.getString(3);
				String oldPhone = rs.getString(4);

				System.out.println(
						"\n***Note: Don't want to change a particular value, directly submit with entering anything");

				System.out.println("Old Name: " + oldName);
				System.out.print("New Name: ");
				String newName = input.readLine();

				System.out.println("Old Email: " + oldEmail);
				System.out.print("New Email: ");
				String newEmail = input.readLine();

				System.out.println("Old Phone: " + oldPhone);
				System.out.print("New Phone: ");
				String newPhone = input.readLine();

				if (Objects.nonNull(newName) && newName.length() > 3) {
					oldName = newName;
				}
				if (Objects.nonNull(newEmail) && newEmail.length() > 10) {
					oldEmail = newEmail;
				}
				if (Objects.nonNull(newPhone) && newPhone.length() >= 10) {
					oldPhone = newPhone;
				}

				st.executeUpdate("UPDATE farmers SET name='" + oldName + "', email='" + oldEmail + "', " + "phone='"
						+ oldPhone + "' WHERE farmer_id=" + farmerId);
			}
		} catch (Exception e) {

		}

	}

	private static void addFarmer() {
		try {
			System.out.println("      Enter New Farmer Details");
			System.out.println("     ==========================");

			System.out.print("Name: ");
			String name = input.readLine();

			System.out.print("Email: ");
			String email = input.readLine();

			System.out.print("Phone: ");
			String phone = input.readLine();

			String SQL = "INSERT INTO farmers VALUES(0, '" + name + "', '" + email + "', '" + phone
					+ "', current_date())";

			st = con.createStatement();
			st.executeUpdate(SQL);

			printLine('=', 40);
			System.out.println("Farmer Details added Successfully");
			printLine('=', 40);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (Objects.nonNull(st)) {
				try {
					st.close();
				} catch (Exception e) {

				}
			}
		}
	}

	private static void viewFarmers() {
		try {
			st = con.createStatement();
			rs = st.executeQuery("SELECT farmer_id ID, name NAME, email EMAIL, phone PHONE FROM farmers");

			if (rs.next()) {

				ResultSetMetaData rsmd = rs.getMetaData();

				printLine('+', 87);
				System.out.format("| %4s | %-25s | %-30s | %15s |%n", 
						rsmd.getColumnLabel(1),
						rsmd.getColumnLabel(2),
						rsmd.getColumnLabel(3),
						rsmd.getColumnLabel(4));

				printLine('+', 87);

				do {
					System.out.format("| %4d | %-25s | %-30s | %15s |%n", rs.getInt(1), rs.getString(2),
							rs.getString(3), rs.getString(4));
				} while (rs.next());

				printLine('+', 87);
			} else {
				System.out.println("No Data Exists");
			}

			input.readLine();

			/*
			 * while (rs.next()) { System.out.format(" %4d | %-25s | %-30s | %15s%n",
			 * rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)); }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (Objects.nonNull(st)) {
				try {
					st.close();
				} catch (Exception e) {

				}
			}
		}
	}

	private static void printMenu() {
		System.out.println("\n      Farmer Operations Menu");
		System.out.println("     ========================");
		System.out.println("1. View \n2. Add\n3. Update\n4. Remove\n5. Exit");

		System.out.print("Your Option: ");
	}

	private static void printLine(char c, int length) {
		for (int i = 0; i < length; i++) {
			System.out.print(c);
		}
		System.out.println();
	}
}
