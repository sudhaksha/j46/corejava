package com.guruofjava.sudaksha.j45.io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

public class ReaderWriterDemo {
	public static void main(String[] args) {

		FileReader in = null;
		FileWriter out = null;

		try {

			in = new FileReader("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\Pivotal_EDU_SpringBootDeveloper_TrainingBrief.pdf");
			out = new FileWriter("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\Pivotal_EDU_SpringBootDeveloper_TrainingBrief_1.pdf");

			int i;

			while ((i = in.read()) != -1) {
				System.out.print((char) i);

				out.write(i);
			}

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			// if (in != null) {
			if (Objects.nonNull(in)) {
				try {
					in.close();
				} catch (IOException ioe) {

				}
			}

			if (Objects.nonNull(out)) {
				try {
					out.close();
				} catch (IOException ioe) {

				}
			}
		}
	}
	
	public static void main1(String[] args) {

		FileReader in = null;

		try {

			in = new FileReader("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\sample.txt");

			int i;

			while ((i = in.read()) != -1) {
				System.out.print((char) i);
			}

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			// if (in != null) {
			if (Objects.nonNull(in)) {
				try {
					in.close();
				} catch (IOException ioe) {

				}
			}
		}
	}
}
