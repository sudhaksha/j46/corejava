package com.guruofjava.sudaksha.j45.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Objects;

public class FileDemo {
	public static void main(String[] args) {
		PrintWriter out = null;
		
		try {
			File res2 = File.createTempFile("temp3", "txt",
					new File("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\"));
			res2.deleteOnExit();

			 out = new PrintWriter(res2);
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			
			String data = "";
			
			while( !(data = input.readLine()).equals("exit")) {
				out.println(data);
				out.flush();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(Objects.nonNull(out)) {
				try {
					out.close();
				}catch(Exception e) {
					
				}
			}
		}

		System.out.println("The END");
	}

	public static void main1(String[] args) throws IOException {
		File res = new File("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\temp1.txt");

		System.out.println(res.exists());
		System.out.println(res.isDirectory());
		System.out.println(res.length());
		System.out.println(new Date(res.lastModified()));

	}
}
