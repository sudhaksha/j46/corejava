package com.guruofjava.sudaksha.j45.io;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

public class DataStreamsDemo {
	public static void main(String[] args) {

		DataInputStream in = null;

		try {
			in = new DataInputStream(new FileInputStream("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\data.txt"));

			System.out.println(in.readByte());
			System.out.println(in.readInt());
			System.out.println(in.readLong());
			System.out.println(in.readFloat());
			System.out.println(in.readDouble());
			System.out.println(in.readUTF());
			System.out.println(in.readChar());
			System.out.println(in.readShort());
			System.out.println(in.readBoolean());

		} catch (IOException ioe) {

		} finally {
			if (Objects.nonNull(in)) {
				try {
					in.close();
				} catch (IOException ioe) {

				}
			}
		}
	}

	public static void main1(String[] args) {
		byte b = 125;
		short s = 879;
		int i = 91234;
		long t = 671234567L;
		float f = 456.123F;
		double d = 89786756.1234098;
		boolean bool = true;
		char c = 'J';
		String str = "James";

		DataOutputStream out = null;

		try {
			out = new DataOutputStream(
					new FileOutputStream("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\data.txt"));

			out.writeByte(b);
			out.writeInt(i);
			out.writeLong(t);
			out.writeFloat(f);
			out.writeDouble(d);
			out.writeUTF(str);
			out.writeChar(c);
			out.writeShort(s);
			out.writeBoolean(bool);
		} catch (IOException ioe) {

		} finally {
			if (Objects.nonNull(out)) {
				try {
					out.close();
				} catch (IOException ioe) {

				}
			}
		}
	}
}
