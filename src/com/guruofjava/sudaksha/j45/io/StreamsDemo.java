package com.guruofjava.sudaksha.j45.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

public class StreamsDemo {
	public static void main3(String[] args) {

		FileInputStream in = null;

		try {
			File inFile = new File("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\sample.txt");

			in = new FileInputStream(inFile);

			int i;

			while ((i = in.read()) != -1) {
				System.out.print((char) i);
			}

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			// if (in != null) {
			if (Objects.nonNull(in)) {
				try {
					in.close();
				} catch (IOException ioe) {

				}
			}
		}
	}

	public static void main(String[] args) {

		try (FileInputStream in = new FileInputStream(
				"C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\sample.txt")) {

			int i;

			while ((i = in.read()) != -1) {
				System.out.print((char) i);
			}

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		}
	}

	public static void main1(String[] args) {

		FileInputStream in = null;

		try {
			in = new FileInputStream("C:\\Users\\rkvod\\RKV\\TECHs\\TRAININGS\\Sudaksha\\Batches\\J45\\sample.txt");

			int i;

			while ((i = in.read()) != -1) {
				System.out.print((char) i);
			}

		} catch (FileNotFoundException fnfe) {

		} catch (IOException ioe) {

		} finally {
			// if (in != null) {
			if (Objects.nonNull(in)) {
				try {
					in.close();
				} catch (IOException ioe) {

				}
			}
		}
	}
}
