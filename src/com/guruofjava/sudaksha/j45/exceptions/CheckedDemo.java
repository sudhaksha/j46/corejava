package com.guruofjava.sudaksha.j45.exceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class CheckedDemo {
	public static void main(String[] args) {

		try {
			m1();
		} catch (FileNotFoundException fnfe) {
			System.out.println("Problem with the given file");
		}

		System.out.println("The END");
	}

	public static void m1() throws FileNotFoundException {
		FileReader in = new FileReader("some file path");
	}
}
