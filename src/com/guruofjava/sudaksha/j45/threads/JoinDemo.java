package com.guruofjava.sudaksha.j45.threads;

public class JoinDemo {
	public static void main(String[] args) {
		String name = Thread.currentThread().getName();

		JoinSupport t1 = new JoinSupport();
		t1.start();

		for (int i = 1; i < 10; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ie) {

			}

			System.out.println(name + ": " + i);

			if (i == 5) {
				try {
					t1.join();
				} catch (InterruptedException ie) {

				}
			}
		}
	}
}
