package com.guruofjava.sudaksha.j45.threads;

public class Consumer extends Thread {
	private Warehouse wc;

	public Consumer(Warehouse wc) {
		this.wc = wc;
	}

	@Override
	public void run() {
		String name = Thread.currentThread().getName();

		for (int i = 0; i < 10; i++) {
			System.out.println(name + ": " + wc.getGoods());
			
			try {
				Thread.sleep(4000);
			} catch (InterruptedException ie) {

			}
		}
	}
}
