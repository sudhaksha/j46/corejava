package com.guruofjava.sudaksha.j45.threads;

public class CurrentThreadDemo {
	public static void main(String[] args) {
		Thread t1 = Thread.currentThread();
		
		System.out.println(t1.getName());
		System.out.println(t1.getPriority());
		System.out.println(t1.getId());
		System.out.println(t1.isAlive());
		System.out.println(t1.isDaemon());
		System.out.println(t1.getState());
	}
}
