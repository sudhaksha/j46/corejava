package com.guruofjava.sudaksha.j45.threads;

public class TestThread extends Thread {
	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		
		for (int i = 1; i <= 10; i++) {
			System.out.println(name + ": " + i);
		}
	}
}
