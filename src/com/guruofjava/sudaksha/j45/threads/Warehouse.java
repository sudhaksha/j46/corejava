package com.guruofjava.sudaksha.j45.threads;

import java.util.Objects;

public class Warehouse {
	private Integer goods;

	public Warehouse() {
		super();
	}

	public synchronized Integer getGoods() {
		Integer temp = null;

		while (true) {
			if (Objects.nonNull(goods)) {
				temp = goods;
				goods = null;
				break;
			} else {
				try {
					wait();
				} catch (InterruptedException ie) {

				}
			}
		}

		notify();
		return temp;
	}

	public synchronized void setGoods(Integer goods) {
		while (true) {
			if (Objects.isNull(this.goods)) {
				this.goods = goods;
				notify();
				return;
			} else {
				try {
					wait();
				} catch (InterruptedException ie) {

				}
			}
		}
	}

}
