package com.guruofjava.sudaksha.j45.threads;

public class ProducerConsumerDemo {
	public static void main(String[] args) {
		Warehouse wm = new Warehouse();
		
		Producer p1 = new Producer(wm);
		Consumer c1 = new Consumer(wm);
		
		p1.setName("Pro1");
		c1.setName("Con1");
		
		p1.start();
		c1.start();
		
	}
}
