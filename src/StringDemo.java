
public class StringDemo {
	public static void main(String[] args) {
		String s1 = "java programming language";		
		System.out.println(s1);
		
		String s2 = "Java Programming Language";
		System.out.println(s2);
		
		System.out.println(s1.toUpperCase());
		System.out.println(s1);
		
		String s3 = s1.toUpperCase();
		System.out.println(s3);
		
		System.out.println(s1.equals(s3));
		System.out.println(s1.equals(s2));
		System.out.println(s1.equalsIgnoreCase(s2));
	}
	
	public static void main1(String[] args) {
		String s = "james gosling";
		
		System.out.println(s);
		
		System.out.println(s.length());
		System.out.println(s.charAt(6));
		
		System.out.println(System.identityHashCode(s));
		
		s = s + " created Java";
		System.out.println(s);
		System.out.println(System.identityHashCode(s));
	}
}
