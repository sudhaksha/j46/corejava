
public class BookDemo {
	public static void main(String[] args) {
		Book b1 = new Book("Java, How To Program", 465);
		Book b2 = new Book("Java, Complete Reference", 580);
		
		System.out.println(b1.getCurrentPage());
		
		System.out.println(Book.MINIMUM_PAGES);
	}
}
